const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	userId: {
		type: String,
		required: [true, "Buyer is required"]
	},
	productId: {
		type: String,
		required: [true, "Product is required"]
	},
	price: {
		type: Number,
		required: [false, "Price is required"]
	},
	purchasedOn: {
		type: Date,
		default: new Date()
	}
})


module.exports = mongoose.model("Order", orderSchema);

