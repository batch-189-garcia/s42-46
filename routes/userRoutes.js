const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");
const jwt = require('jsonwebtoken');

// User registration
router.post("/register", (req, res) => {
    userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// User registration
router.post("/registerwch", (req, res) => {
    userController.registerUserwch(req.body).then(resultFromController => res.send(resultFromController));
});

// User Authentication Start
router.post("/login", (req, res) => {
    userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Set user as Admin
router.put("/:userId/setadmin", auth.verify, (req, res) => {

            // token entered on postman
            const userData = auth.decode(req.headers.authorization);

            console.log(userData);

            if (userData.isAdmin) {

                userController.updateUser(req.params, req.body).then(resultFromController => res.send(resultFromController))

            } else {

                res.send({ auth: "failed" });
    }

})

// Non-admin User checkout (Create Order) 
router.post("/order", (req, res) => {

    let data = {
        userId: req.body.userId,
        productId: req.body.productId
    }

    userController.order(data).then(resultFromController => res.send(resultFromController))

});

// Non-admin User checkout (Create Order) 
router.post("/auth-order", (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    console.log(userData);
    
    if (userData.isAdmin) {

        res.send({ auth: "User is Admin, not allowed" });

    } else {

        let data = {
        userId: userData.id,
        productId: req.body.productId
    }

    userController.order(data).then(resultFromController => res.send(resultFromController))

    }

});


// Route for Retrieving authenticated user’s orders
router.get("/details", auth.verify, (req, res) => {
    
    const userData = auth.decode(req.headers.authorization);
    
     userController.detailsUser({id : userData.id}).then(resultFromController => res.send(resultFromController));
})

// Additional Feature - Retrieve all users (Admin)
router.get("/admin-all", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    userController.getAllUsers(userData).then(resultFromController => res.send(resultFromController))
})

module.exports = router;