const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require("../auth");
const orderController = require('../controllers/orderController'); 
const productController = require('../controllers/productController');


const jwt = require('jsonwebtoken');

// Retrieve all orders (Admin only)
router.get("/admin-all", auth.verify, (req, res) => {

    const userData = auth.decode(req.headers.authorization);

    orderController.getAllOrders(userData).then(resultFromController => res.send(resultFromController))
})

module.exports = router;