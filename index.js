const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/userRoutes');
const productRoutes = require('./routes/productRoutes');
const orderRoutes = require('./routes/orderRoutes');
const dotenv = require('dotenv');
const bodyParser = require('body-parser');

dotenv.config();

const port = 8888;

const app = express();


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));

app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes);

app.use(bodyParser.urlencoded({ extended: true }));

app.set('view engine', 'ejs');

mongoose.connect(`mongodb+srv://admin:${process.env.MONGODB_PASSWORD}@zuitt-bootcamp.tveytx1.mongodb.net/s42-s46?retryWrites=true&w=majority`,{
	useNewUrlParser:true,
	useUnifiedTopology: true
});

let db = mongoose.connection;

db.on('error', () => console.error.bind(console, 'error'));
db.once('open', () => console.log('Now connected to MongoDB Atlas'));


const Product = require('./models/Product');
const User = require('./models/User');

app.get('/ht/list', (req, res) => {
    Product.find({}, function(err, product) {
        res.render('index', {
            productList: product
        })
    })
})

app.get('/ht/listactive', (req, res) => {
    Product.find({ isActive: true }, function(err, product) {
        res.render('active', {
            productList: product
        })
    })
})

/*app.get('/ht/:productId', (req, res) => {
    var Id = req.params.productId;
    Product.findById((Id), function(err, product) {
        res.render('singleproduct', {
            productList: product
        })
    })
})*/

/*app.post("/", function(req, res) {
    // res.render('post', {
            
    //     })
    let newUser = new User({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        email: req.body.email,
        password: req.body.password,
        mobileNo: req.body.mobileNo,
        
    });
    
    newUser.save();
    res.redirect('/');
})*/

app.listen(process.env.PORT || port, () => {
	console.log(`API is now online on port ${process.env.PORT || port}`);
})

