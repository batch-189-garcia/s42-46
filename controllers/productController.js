const Product = require('../models/Product');
const User = require('../models/User');
const ejs = require('ejs');
const bodyParser = require('body-parser');

// Create Product (Admin only)
module.exports.addProduct = (reqBody) => {

    let newProduct = new Product({
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    });

    return newProduct.save().then((product, error) => {

        if (error) {

            return false

        } else {

            return true
        }
    })
}

// Retrieve all active products 
module.exports.getAllActive = () => {

    return Product.find({ isActive: true }).then(result => {

        return result
    })
}

// Addt Feature - Route to Retrieve all products (Admin Only)
module.exports.getAllProducts = () => {

    return Product.find({}).then(result => {
        
        return result

    })
    .catch(err => {
        return ("Failed")
    })
}

// Route to Retrieve single product
module.exports.getProduct = (reqParams) => {

    return Product.findById(reqParams.productId).then((result, error) => {

        if (error) {
            return false

        } else {
            return result
        }
    })
}

// Update Product information (Admin only)
module.exports.updateProduct = (reqParams, reqBody) => {

    let updatedProduct = {

        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price
    }

    return Product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((product, error) => {

        if (error) {
            return false

        } else {
            return true
        }
    })

}

// Archive Product (Admin only)
module.exports.deactivateProduct = (reqParams, reqBody) => {

    let deactivateProduct = {

        isActive: reqBody.isActive
    }

    return Product.findByIdAndUpdate(reqParams.productId, deactivateProduct).then((product, error) => {

        if (error) {
            return false

        } else {
            return true
        }
    })

}