const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');
const Order = require('../models/Order');
const orderController = require('../controllers/orderController');

// User Registration 
module.exports.registerUser = (reqBody) => {


    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNo: reqBody.mobileNo,
        password: bcrypt.hashSync(reqBody.password, 10)
    })

    return newUser.save().then((user, error) => {

        if (error) {
            return false

        } else {
            return true
        }
    })
}

// User Registration 
module.exports.registerUserwch = (reqBody) => {

    // The result is sent back to the frontend via the "then" method found in the route file
    return User.find({email: reqBody.email}).then(result => {

        // The "find" method returns a record if a match is found
        if(result.length > 0) {
            return ("Email Address already used")
        
        // No duplicate email found
        // The user is not yet registered in the database
        } else {
            let newUser = new User({
                firstName: reqBody.firstName,
                lastName: reqBody.lastName,
                email: reqBody.email,
                mobileNo: reqBody.mobileNo,
                password: bcrypt.hashSync(reqBody.password, 10)
            })

            return newUser.save().then((user, error) => {

                if (error) {
                    return false

                } else {
                    return (`User ${newUser.firstName} ${newUser.lastName} is now registered`)
                }
            })
        }
    })    
}

// User Authentication 
module.exports.loginUser = (reqBody) => {

    return User.findOne({ email: reqBody.email }).then(result => {

        if (result == null) {
            return false

        } else {

            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

            if (isPasswordCorrect) {

                return { access: auth.createAccessToken(result) }

            } else {
                return false
            }
        }
    })
}



// Set user as Admin 
module.exports.updateUser = (reqParams, reqBody) => {

	let updateUser = {

		isAdmin: reqBody.isAdmin
	}


	return User.findByIdAndUpdate(reqParams.userId, updateUser).then((user, error) => {

		if(error) {
			return false

		} else {
			return true
		}
	})

}


// Non-admin User checkout (Create Order)  
module.exports.order = async (data) => {

    return User.findById(data.userId).then(result => {

            return Product.findById(data.productId).then(result => {

                    let isUserUpdated = User.findById(data.userId).then(user => {

                        user.orders.push({ productId: data.productId });

                        return user.save().then((user, error) => {

                            if (error) {
                                return false
                            } else {

                                return ("Order created by Non Admin User")
                            }

                        })

                    })

                    let isProductUpdated = Product.findById(data.productId).then(product => {

                        product.buyers.push({ userId: data.userId });

                        return product.save().then((product, error) => {

                            if (error) {
                                return false
                            } else {
                                return ("Order created by Non Admin User")
                            }
                        })

                    })

                    let isOrderUpdated = Product.findById(data.productId).then(product => {

                        let newOrder = new Order({
                            userId: data.userId,
                            productId: product._id,
                            price: product.price
                        })

                        return newOrder.save().then((user, error) => {

                            if (error) {
                                return false

                            } else {
                                return ("Order created by Non Admin User")
                            }
                        })

                    })

                    if (isUserUpdated && isProductUpdated) {

                        // console.log("buyers "+product.buyers)
                        return ("Order created by Non Admin User")

                    } else {

                        return false

                    }

                    return ("Order created by Non Admin User")
                })
                .catch(err => {
                    return ("Product ID not found")
                })
        })
        .catch(err => {
            return ("User ID not found")
        })

}

// Retrieve authenticated user’s orders
module.exports.detailsUser = (data) => {

    return User.findById(data.id).then((result, error) => {


        if (error) {
            return error
        }
        console.log(result)



        return result.orders;
    })
}

// Retrieve all orders (Admin)
module.exports.getAllUsers = async (data) => {

    if (data.isAdmin) {

        return User.find({}).then(result => {

            return result

        })
    } else {

        return false
    }
}