const User = require('../models/User');
const Product = require('../models/Product');
const productController = require('../controllers/productController');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// Retrieve all orders (Admin only)
module.exports.getAllOrders = async (data) => {

    if (data.isAdmin) {

        return Order.find({}).then(result => {

            return result

        })
    } else {
        
        return false
    }
}